import contextlib
import tempfile

import msoffcrypto
import xlrd
import io
import pandas as pd
import openpyxl

class Xls(object):

    def returnPassWordXlx(name):
        aARRAY = dict( [
            
            ("AREA CRC", "C4wn7@qD8P"),
            ("AREA VENDAS", "FQnpe@xdn4"),
            ("ASP", "rm69@32gn"),
            ("AzureDevOps", "Hgjf@445d"),
            ("BSCSIX", "OKna@841"),
            ("CATI BO", "73Hgf@lO8p"),
            ("CBCF", "fDss3@pL9o"),
            ("CCMP", "A)f$00ay"),
            ("CLARIFY_VENDITA", "O276l@lzG0"),
            ("COLISEU", "Rtd@2340"),
            ("CUIC", "Pl1j&0!9"),
            ("DISTRIBUIDOR DE CHAMADOS", "Dtbi%489"),
            ("EASY_VENDAS", "Rsgt$128"),
            ("ECM", "Q)90O&wwP"),
            ("ERICSSON_CAS", "Rcf@541"),
            ("FATURA ELETRONICA CSR", "boF2@8UW4"),
            ("FILE SERVER", "Shj@78s"),
            ("GFA", "Rtmu*157"),
            ("GWT", "-1#)d4T0"),
            ("InfoAcesso", "aPr4&vCq9"),
            ("INTEGRA", "Pa3E@rM0y"),
            ("INTRANET CRC CLICKTIM", "v1UNb@Lzu"),
            ("METAFRAME", "gf78T@hf4Ds"),
            ("MUNDO TIM GESTAO NEGOCIOS", "PgE4x@33Fd"),
            ("MUNDO TIM LIVE", "uXXbJ@n1hy"),
            ("NICE", "iG(s@t5M"),
            ("PACOTE TRANFERENCIA1", ""),

            ("PHOENIX", "n321c@Yg36"),

            ("PORTAL CORPORATIVO E RESIDENCIAL", "RrT3@oPPd"),

            ("PORTAL GESTAO DE VENDAS", "tNxy$249"),
            ("PORTAL LOGISTICA", "boF2@8UW4"),
            ("PORTAL TRADE CONSUMER", "RrT3@oPPd"),
            ("PORTAL PARCELAMENTO", "Lq4W#x8mR"),
            ("PPM", "TrrE#155"),
            ("SAP CLM", "TrrE#155"),
            ("SAS94", "FgB0$r6Ti"),
            ("SERVCEL", "SVEI9@Ct1W"),
            ("SIEBEL POS", "Rstu#159"),
            ("SIEBEL", "s9Kd@b13J"),
            ("SISJUR TOKEN", "Ju1i@P@0"),
            ("TFS", "RtfV#522"),
            ("TIM CAPTURE", "oWfY@099"),
            ("TIMVENDAS", "Hgnd*1547"),
            ("VCE", "ONI3l@7PX0"),
            ("VERO", "oWfY@099")
        ]
        )
        cfile = "%s" % (name)

        cpassword = ""
        try:
            cpassword = aARRAY[cfile]
        except:
            cpassword = "VelvetSweatshop"

        return cpassword

    # based on: https://stackoverflow.com/a/52290873/9560908
    @contextlib.contextmanager
    def handle_protected_workbook(wb_filepath,name):
        try:
            with xlrd.open_workbook(wb_filepath) as wb:
                yield wb
        except xlrd.biffh.XLRDError as e:
            if str(e) != "Workbook is encrypted":
                raise
            # Try and unencrypt workbook with magic password
            wb_path = Path(wb_filepath)
            with wb_path.open("rb") as fp:
                wb_msoffcrypto_file = msoffcrypto.OfficeFile(fp)
                try:
                    # Yes, this is actually a thing
                    # https://nakedsecurity.sophos.com/2013/04/11/password-excel-velvet-sweatshop/
                    wb_msoffcrypto_file.load_key(password=returnPassWordXlx(name))
                except Exception as e:
                    raise Exception('Unable to read file "{}"'.format(wb_path)) from e
                    
                # Magic Excel password worked
                with tempfile.NamedTemporaryFile(delete=False) as tmp_wb_unencrypted_file:
                    # Decrypt into the tempfile
                    wb_msoffcrypto_file.decrypt(tmp_wb_unencrypted_file)
                    decrypted_path = Path(tmp_wb_unencrypted_file.name)
                try:
                    with xlrd.open_workbook(str(decrypted_path)) as wb:
                        yield wb
                finally:
                    decrypted_path.unlink()

    def GetFileExcel(sheet,file_excel,name):

        a = Xls
        def FileExcel(file_excel,password):
            file = msoffcrypto.OfficeFile(open(file_excel,"rb"))

            #use password
            file.load_key(password = a.returnPassWordXlx(name))

            descrypted = io.BytesIO()
            df = file.decrypt(descrypted)
            return df

        try:
            xls = pd.read_excel(file_excel,sheet_name=sheet)
        except:
            password = a.returnPassWordXlx(name)
            xls = FileExcel(file_excel,password)

        json_str = xls.to_json(orient='records')
        return json_str

    def UpdateFileExcel(sheet,file_excel,name,rowcol,textupdate):
        try:
            wb = openpyxl.load_workbook(file_excel)
            # wb.security.workbook_password = returnPassWordXlx("name")
            # wb.active
        except:
            return [{"result":"File %s not find" % (file_excel)}]

        try:
            ws = wb[sheet]
            ws[rowcol] = textupdate
            wb.save(file_excel)
        except:
            return [{"result":"Sheet %s not find" % (sheet)}]

        return [{"result":"Updating success!" }]